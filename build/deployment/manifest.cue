package deployment

import (
	s	".../service:service"
)
#Deployment: {

	name: "deployment"
	artifact: s.#Artifact

	config: {
		parameter: {
			cgrf_config: {
				access_key: "radiatus"
				secret_key: "radiatus123"
				advanced_config: "false"
				advanced_config_root: "/radiatus"
				use_auth: "false"
			}
			cprm_config: {
				advanced_config: "false"
				advanced_config_root: "/radiatus"
				use_auth: "false"
			}

			cgrf_config: {
				"_deps": {"apt":[],"python":[],"commands":[]}
			}
			cprm_config: {
				"_deps": {"apt":[],"python":[],"commands":[]}
			}
		}

		resource: {
			gitlabRegistryAccessToken: secret: "admin/registryaccesstoken"
			gitlabRegistryAccessToken: secret: "admin/registryaccesstoken"
			r5m4transactgrafanadata73737c8073624924b98ccfc67869b4f3: volume: "admin/r5m4transactgrafanadata73737c8073624924b98ccfc67869b4f3"
			r5m4transactprometheusdata51ecf95357a64aabb16c0a1263b6a353: volume: "admin/r5m4transactprometheusdata51ecf95357a64aabb16c0a1263b6a353"
		}

		scale: detail: {
			cgrf: hsize: 1
			cprm: hsize: 1
		}
		
		resilience: 0
	}
}
