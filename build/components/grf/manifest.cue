package component

import k "kumori.systems/kumori:kumori"

#Artifact: {

	ref: name: "components/grf"

	description: {

		srv: {
			server: segrafe: {
				protocol: "http",
				port: 10000
			}

			server: tegrfst: {
				protocol: "http"
				port: 9998
			}

			client: cltprmprsrv: {
				protocol: "tcp"
			}
			client: cltelserest: {
				protocol: "tcp"
			}
			client: cltmqlprotc: {
				protocol: "tcp"
			}
			client: cltpslpslsr: {
				protocol: "tcp"
			}
        }

		config: {
			parameter: {
				app_config: {
					grafana_instances: number | *1
					user: string | *"guest"
					password: string | *"guest"
					use_auth: string | *"false"
					access_key: string | *"radiatus"
					secret_key: string | *"radiatus123"
					advanced_config: string | *"false"
					advanced_config_root: string | *"/radiatus"
					use_auth: string | *"false"
					auth: {
						api_url: string
						login_url: string
						manager_id: string
						tenant: string
						tenant_secret: string
						component: string
						permission_cache_timeout: number
						url_filters: [string]: [string]: [string]
					} | *{}
					#apt_struct: {
						name: string
						pre_install_commands: [...string]
						post_install_commands: [...string]
					}
					#python_struct: {
						name: string
						version?: string
						installer: "python2-pip"|"python3-pip"
						package_url?: string
					}
					"_deps":{
						apt: [...#apt_struct] | *[]
						python: [...#python_struct] | *[]
						commands: [...string]
					}
					grafe_ep_port: number | *10000
					grfst_port: number | *9998
					prsrv_port: number | *11001
					erest_port: number | *11000
					protc_port: number | *10000
					pslsr_port: number | *10001
				}
			}
			resource: {
				gitlabRegistryAccessToken: k.#Secret
				r5m4transactgrafanadata73737c8073624924b98ccfc67869b4f3: k.#Volume
			}
		}

		size: {
			bandwidth: { size: 10, unit: "M" }
		}

		probe: cgrf: {
			liveness: {
				protocol: exec: {
					path: "/radiatus/component/liveness.sh 3000"
				}
				frequency: "medium"
				startupGraceWindow: {
					unit: "ms"
					duration: 1000
					probe: true
				}
				timeout: 30000
			}

			readiness: {
				protocol: exec: {
					path: "/radiatus/component/readiness.sh 3000"
				}
				frequency: "medium"
				timeout: 30000
			}
		}

		code: {
			cgrf: {
				name: "cgrf"
				image: {
					hub: {
						name: "docker.iti.upv.es"
						secret: "gitlabRegistryAccessToken"
					}
					tag: "radiatus/radiatus5/grafana:1.1.3"
				}

				mapping: {
					filesystem: {
						"/radiatus/config.json": {
							data: value: description.config.parameter.app_config
							format: "json"
						},
						"/data": {
							volume: "r5m4transactgrafanadata73737c8073624924b98ccfc67869b4f3"
						}
					}
					env: {

					}
				}
				size: {
					memory: { size: 3000, unit: "M" }
					mincpu: 500
					cpu: { size: 500, unit: "m" }
				}
			}
		}

	}
}
