package component

import (
  k  "kumori.systems/kumori/@1.1.6:kumori"
)


#Artifact: {

	ref: name: "components/prm"

	description: {

		srv: {
			server: seprmui: {
				protocol: "http",
				port: 10001
			}
			server: seprmfl: {
				protocol: "http",
				port: 10002
			}

			server: teprmst: {
				protocol: "http"
				port: 9998
			}
			server: svprsrv: {
				protocol: "http"
				port: 11001
			}

			client: cltprmprsrv: {
				protocol: "tcp"
			}
			client: cltelserest: {
				protocol: "tcp"
			}
			client: cltmqlprotc: {
				protocol: "tcp"
			}
			client: cltpslpslsr: {
				protocol: "tcp"
			}
			client: clttno:{
				protocol: "tcp"
			}
        }

		config: {
			parameter: {
				app_config: {
					prometheus_instances: number | *1
					user: string | *"guest"
					password: string | *"guest"
					use_auth: string | *"false"
					advanced_config: string | *"false"
					advanced_config_root: string | *"/radiatus"
					use_auth: string | *"false"
					auth: {
						api_url: string
						login_url: string
						manager_id: string
						tenant: string
						tenant_secret: string
						component: string
						permission_cache_timeout: number
						url_filters: [string]: [string]: [string]
					} | *{}
					#apt_struct: {
						name: string
						pre_install_commands: [...string]
						post_install_commands: [...string]
					}
					#python_struct: {
						name: string
						version?: string
						installer: "python2-pip"|"python3-pip"
						package_url?: string
					}
					"_deps":{
						apt: [...#apt_struct] | *[]
						python: [...#python_struct] | *[]
						commands: [...string]
					}
					prmui_ep_port: number | *10001
					prmfl_ep_port: number | *10002
					prmst_port: number | *9998
					prsrv_port: number | *11001
				}
			}
			resource: {
				gitlabRegistryAccessToken: k.#Secret
				r5m4transactprometheusdata51ecf95357a64aabb16c0a1263b6a353: k.#Volume
			}
		}

		size: {
			bandwidth: { size: 10, unit: "M" }
		}

		probe: cprm: {
			liveness: {
				protocol: exec: {
					path: "/radiatus/component/liveness.sh 30000"
				}
				frequency: "medium"
				startupGraceWindow: {
					unit: "ms"
					duration: 3000
					probe: true
				}
				timeout: 30000
			}

			readiness: {
				protocol: exec: {
					path: "/radiatus/component/readiness.sh 30000"
				}
				frequency: "medium"
				timeout: 30000
			}
		}

		code: {
			cprm: {
				name: "cprm"
				image: {
					hub: {
						name: "docker.iti.upv.es"
						secret: "gitlabRegistryAccessToken"
					}
					tag: "radiatus/radiatus5/prometheus:1.0.21"
				}

				mapping: {
					filesystem: {
						"/radiatus/config.json": {
							data: value: description.config.parameter.app_config
							format: "json"
						},
						"/data": {
							volume: "r5m4transactprometheusdata51ecf95357a64aabb16c0a1263b6a353"
						}
					}
					env: {

					}
				}
				size: {
					memory: { size: 3000, unit: "M" }
					mincpu: 500
					cpu: { size: 500, unit: "m" }
				}
			}
		}

	}
}
