package service

import (
	k "kumori.systems/kumori:kumori"
	pcgrf ".../components/grf:component"
	pcprm ".../components/prm:component"

)

#Artifact: {

	ref: name: "service"

	description:{

		config:{
			parameter:{
				cgrf_config: {
					use_auth: string
					access_key: string
					secret_key: string
					advanced_config: string
					advanced_config_root: string
				}
				cprm_config: {
					use_auth: string
					advanced_config: string
					advanced_config_root: string
				}

				cgrf_config: {
					#apt_struct: {
						name: string
						pre_install_commands: [...string]
						post_install_commands: [...string]
					}
					#python_struct: {
						name: string
						version?: string
						installer: "python2-pip"|"python3-pip"
						package_url?: string
					}
					"_deps":{
						apt: [...#apt_struct] | *[]
						python: [...#python_struct] | *[]
						commands: [...string]
					}
				}
				cprm_config: {
					#apt_struct: {
						name: string
						pre_install_commands: [...string]
						post_install_commands: [...string]
					}
					#python_struct: {
						name: string
						version?: string
						installer: "python2-pip"|"python3-pip"
						package_url?: string
					}
					"_deps":{
						apt: [...#apt_struct] | *[]
						python: [...#python_struct] | *[]
						commands: [...string]
					}
				}
			}

			resource: {
				gitlabRegistryAccessToken: k.#Secret
				gitlabRegistryAccessToken: k.#Secret
				r5m4transactgrafanadata73737c8073624924b98ccfc67869b4f3: k.#Volume
				r5m4transactprometheusdata51ecf95357a64aabb16c0a1263b6a353: k.#Volume
			}
		}

		role:{
			cgrf: {
				artifact: pcgrf.#Artifact
				config: {
					parameter: {
						app_config: description.config.parameter.cgrf_config
					}
					resource: {
						gitlabRegistryAccessToken: description.config.resource.gitlabRegistryAccessToken
						r5m4transactgrafanadata73737c8073624924b98ccfc67869b4f3: description.config.resource.r5m4transactgrafanadata73737c8073624924b98ccfc67869b4f3
					}
				}
			}
			cprm: {
				artifact: pcprm.#Artifact
				config: {
					parameter: {
						app_config: description.config.parameter.cprm_config
					}
					resource: {
						gitlabRegistryAccessToken: description.config.resource.gitlabRegistryAccessToken
						r5m4transactprometheusdata51ecf95357a64aabb16c0a1263b6a353: description.config.resource.r5m4transactprometheusdata51ecf95357a64aabb16c0a1263b6a353
					}
				}
			}
		}

		srv:{
			server: {
				cgrfsegrafe: {
					protocol:"http",
					port:10000
				}
				cprmseprmui: {
					protocol:"http",
					port:10001
				}
				cprmseprmfl: {
					protocol:"http",
					port:10002
					}
			}
			client: cprmcetno: {
				protocol: "http"
			}
		}

		connect: {
			cgrfsegrafex: {
				as: "lb"
				from: self: "cgrfsegrafe"
				to: cgrf: "segrafe": _
			}
			cprmseprmuix: {
				as: "lb"
				from: self: "cprmseprmui"
				to: cprm: "seprmui": _
			}
			cprmseprmflx: {
				as: "lb"
				from: self: "cprmseprmfl"
				to: cprm: "seprmfl": _
			}
			cprmcetnox: {
				as: "lb"
				from: cprm: "clttno"
				to: self: "cprmcetno": _
			}

			cprmsvprsrvx: {
				as: "full"
				from: {
					cgrf: "cltprmprsrv"
				}
				to: cprm: "svprsrv": _
			}


		}
	}
}
